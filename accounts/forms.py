from django.contrib.auth.forms import UserCreationForm, UserChangeForm, AuthenticationForm

from .models import User


class CustomUserCreationForm(UserCreationForm):

    class Meta(UserCreationForm):
        model = User
        fields = ('username','email',)

class CustomUserAuthenticationForm(AuthenticationForm):
    
    class Meta:
        model = User
        # fields = ('email',)

class CustomUserChangeForm(UserChangeForm):
    
    password = None

    class Meta:
        model = User
        fields = ('email','profile_image',)
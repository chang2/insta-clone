from django.db import models
from django.contrib.auth.models import AbstractUser
from django.conf import settings
from imagekit.models import ProcessedImageField
from imagekit.processors import ResizeToFill

class User(AbstractUser):
    follow = models.ManyToManyField(settings.AUTH_USER_MODEL,related_name='follower', blank=True)
    # follow = models.ManyToManyField("self", blank=True)
    profile_image = ProcessedImageField(
                upload_to='posts/images',                # 저장 위치
                processors=[ResizeToFill(150, 150)],     # 처리할 작업 목록
                format='JPEG',                           # 저장 포맷
                options={'quality':90},                  # 옵션
                blank=True
    )
    def __str__(self):
        return self.username






# 예전코드 

# # 국제화를 위한 코드 안써도 됨. 사용한 이유는 장고의 기본 user 내부에서도 똑같이 사용되었기 때문
# # 국제화? 'email address'가 한국어 설정에서는 '이메일'로 변경
# from django.utils.translation import ugettext_lazy as _

# from .managers import CustomUserManager

# class User(AbstractUser):
#     # # 기존의 user 모델은 username이라는 칼럼을 이용해서 로그인을 했음
#     # # email을 이용해서 로그인을 하기 위하여 다음과 같이 코드 작성
#     # # 기존에 있던 user모델도 email 칼럼이 있지만 unique=True 옵션을 주기 위해서 새로 작성
#     # email = models.EmailField(_('email address'), unique=True)

#     # # USERNAME_FIELD 를 아이디 값으로 사용한다.
#     # USERNAME_FIELD = 'email'
#     # REQUIRED_FIELDS = ['username',]
    
#     # # 생성을 관리해주는 Manager를 objects로 등록해야함.
#     # # managers.py 를 만들어서 따로 작성
#     # objects = CustomUserManager()

#     # def __str__(self):
#     #     return self.email
from django.shortcuts import render, redirect
# from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth import login as auth_login
from django.contrib.auth import logout as auth_logout
from .models import User
from .forms import CustomUserAuthenticationForm, CustomUserCreationForm, CustomUserChangeForm
from django.contrib.auth.decorators import login_required

# Create your views here.
def signup(request):
    if request.method=="POST":
        # 사용자를 등록하는 로직
        form = CustomUserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('posts:list')
        
    else: 
        # 정보를 입력하는 폼을 전달
        form = CustomUserCreationForm()
    return render(request,'accounts/signup.html',{'form':form})


def login(request):
    # 이미 로그인 했으면서 또 로그인 페이지 보여달라고 할때
    if request.user.is_authenticated:
        return redirect('posts:list')
        
    #사용자가 로그인을 요청했으면?
    if request.method == "POST": 
        form =  CustomUserAuthenticationForm(request, request.POST)
        if form.is_valid():
            auth_login(request, form.get_user())
            return redirect('posts:list')
    
    #로그인을 위한 폼을 요청했을때?
    else:
        form = CustomUserAuthenticationForm()
    return render(request,'accounts/login.html',{'form':form})

def logout(request):
    auth_logout(request)
    return redirect('posts:list')

def user_page(request,id):
    user_info = User.objects.get(id=id)
    return render(request, 'accounts/user_page.html',{'user_info':user_info})

@login_required
def follow(request,id):
    me = request.user
    you = User.objects.get(id=id)
    if me != you:
        # 사용자가 이미 팔로우를 눌렀으면 취소
        if me.follow.filter(id=you.id).exists():
            me.follow.remove(you)
        # 안눌렀으면 좋아요
        else:
            me.follow.add(you)
    return redirect('accounts:user_page',id)

@login_required
def edit_profile(request,id):
    me = request.user
    you = User.objects.get(id=id)
    if me == you:
        if request.method == "POST":
            form = CustomUserChangeForm(request.POST, request.FILES, instance=me)
            if form.is_valid():
                form.save()
                return redirect('accounts:user_page', id)
        else:
            form = CustomUserChangeForm(instance=me)
            
        return render(request, 'accounts/edit_profile.html', {'form':form})

    return redirect('accounts:user_page',id)
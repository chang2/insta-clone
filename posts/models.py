from django.db import models
from imagekit.models import ProcessedImageField
from imagekit.processors import ResizeToFill
from django.conf import settings

# Create your models here.
class Hashtag(models.Model):
    # 같은 이름의 해시태그는 하나만 존재해야 하기때문에 unique=True
    content = models.TextField(unique=True)

    def __str__(self):
        return self.content

class Post(models.Model):
    content = models.CharField(max_length=100)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    # related_name 을 설정해주는 이유 - 역참조에서 문제발생
    # user 칼럼도 settings.AUTH_USER_MODEL과 관계 설정이 되어있다. 
    likes = models.ManyToManyField(settings.AUTH_USER_MODEL, related_name='like_post_set',blank=True)
    hashtag = models.ManyToManyField(Hashtag, blank=True)

class Image(models.Model):
    post = models.ForeignKey(Post, on_delete=models.CASCADE)
    file = ProcessedImageField(
                upload_to='posts/images',                # 저장 위치
                processors=[ResizeToFill(600, 600)],     # 처리할 작업 목록
                format='JPEG',                           # 저장 포맷
                options={'quality':90},                  # 옵션
    )

class Comment(models.Model):
    content = models.CharField(max_length=100)
    post = models.ForeignKey(Post, on_delete=models.CASCADE)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    
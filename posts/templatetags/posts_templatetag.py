from django import template

register = template.Library()

@register.filter
def hashtag_link(post):
    # 인자로 들어온 post의 content를 가져온다
    content = post.content
    # post가 가지고 있는 hashtag를 모두 가져온다.
    tags = post.hashtag.all()

    # tags의 각각의 인스턴스를(tag)를 순회하며, content 내에서 해당 문자열(해쉬태그)을 => 링크를 포함한 문자열로 replace 한다.
    for tag in tags:
        # 예시) #싸피 => <a href="/posts/hashtag/1/">#싸피</a>
        content = content.replace(f'{tag.content}', f'<a href="/posts/hashtag/{tag.id}/">{tag.content}</a>')
    
    # 원하는 문자열로 치환이 완료된 content를 리턴한다.
    return content 
from django.urls import path
from . import views
app_name = 'posts'

urlpatterns = [
    path('create/', views.create, name="create"),
    path('', views.list, name='list'),
    path('<int:id>/update/', views.update, name='update'),

    path('<int:id>/comment/create/', views.comment_create, name='comment_create'),

    path('<int:id>/like/', views.like, name="like"),

    path('hashtag/<int:id>/', views.hashtag, name='hashtag'),

]
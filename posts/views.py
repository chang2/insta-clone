from django.shortcuts import render, redirect
from .forms import PostForm, ImageForm, CommentForm
from .models import Post, Image, Hashtag
from django.db import transaction
from django.contrib.auth.decorators import login_required

# Create your views here.
def list(request):
    posts = Post.objects.order_by('-id').all()
    comment_form = CommentForm()
    return render(request,'posts/list.html',{'posts':posts, 'comment_form':comment_form})

@login_required
def create(request):
    # 1. get 방식으로 데이터를 입력할 form 을 요청한다.
    # 4. 사용자가 데이터를 입력하여 post 요청을 보낸다.
    # 9. 사용자가 다시 적절한 데어터를 입력하여 post 요청을 보낸다.
    if request.method == 'POST':
        # 5. post방식으로 저장요청을 받고 데이터를 받아 PostForm에 넣어 인스턴스화 한다.
        # 10. 5번과 같음
        post_form = PostForm(request.POST)
        image_form = ImageForm(request.FILES)
        # 6. 데이터 검증을 한다.
        # 11. 6번과 같음
        if post_form.is_valid():
            # 12. 적절한 데이터가 들어온다. 데이터를 저장하고 list 페이지로 리다이렉트 시킨다.
            post = post_form.save(commit=False)
            post.user = request.user
            post.save()
            
            # 해쉬태그 기능 추가
            # is_valid() 함수를 통과한 cleaned_data에서 content를 가져온다.
            content = post_form.cleaned_data.get('content')
            # 내용을 split()함수로 띄어쓰기를 기준으로 잘라서 단어로 만든다
            content_word = content.split()
            for word in content_word:
                # 단어의 첫글자가 #으로 시작하면
                if word[0] == '#':
                    # get_or_create로 해쉬태를 찾고 없으면 생성한다.
                    tag = Hashtag.objects.get_or_create(content=word)
                    # 찾거나 생성한 해쉬태그를 post에 M:N관계로 추가한다.
                    # tag[0]으로 추가하는 이유 : get_or_create의 리턴은 (hashtag객체, True or False) 형태의 튜플이기때문
                    post.hashtag.add(tag[0])
            
            for image in request.FILES.getlist('file'):
                request.FILES['file'] = image
                image_form = ImageForm(request.POST,request.FILES)
                if image_form.is_valid():
                    image = image_form.save(commit=False)
                    image.post = post
                    image.save()
            return redirect('posts:list')  
        else:
            # 7. 적절하지 않은 데이터가 들어온다.
            # 아무내용이 없기 때문에 else문은 생략
            pass
        
    else:
        # 2. PostForm을 인스턴스화 해서 form에 저장한다. 
        post_form = PostForm()
        image_form = ImageForm()
    # 3. form을 담아서 create.html을 보내준다.
    # 8. 사용자가 입력한 데이터는 form에 담아진 상태로 다시 form을 담아서 create.html을 보내준다.
    return render(request,'posts/form.html', {'post_form':post_form, 'image_form':image_form})

@login_required
def update(request,id):
    post = Post.objects.get(id=id)
    if post.user == request.user:
        if request.method == 'POST':
            post_form = PostForm(request.POST, instance=post)
            if post_form.is_valid():
                post_form.save()

                # 기존의 해쉬태그를 전부 삭제
                post.hashtag.clear()
                # 아래는 해쉬태그를 추가하는 로직과 동일
                content = post_form.cleaned_data.get('content')
                content_word = content.split()
                for word in content_word:
                    if word[0] == '#':
                        tag = Hashtag.objects.get_or_create(content=word)
                        post.hashtag.add(tag[0])
                return redirect('posts:list')  
            else:
                pass
        else:
            post_form = PostForm(instance=post)
    else:
        return redirect('posts:list')
    return render(request,'posts/form.html', {'post_form':post_form})

@login_required
def comment_create(request,id):
    post = Post.objects.get(id=id)
    if request.method == "POST":
        form = CommentForm(request.POST)
        if form.is_valid():
            comment = form.save(commit=False)
            comment.user = request.user
            comment.post = post
            comment.save()
            return redirect('posts:list')

@login_required
def like(request,id):
    user = request.user
    post = Post.objects.get(id=id)
    
    # 사용자가 like를 눌렀으면 취소
    if post.likes.filter(id=user.id).exists():
        post.likes.remove(user)
    # 안눌렀으면 좋아요
    else:
        post.likes.add(user)
    # data = {'likes_count' : post.like.count()}
    return redirect('posts:list')

def hashtag(request,id):
    hashtag = Hashtag.objects.get(id=id)
    posts = hashtag.post_set.all()
    # list 페이지를 재활용하기 위해서 CommentForm도 추가하여 list와 동일하게 구성
    comment_form = CommentForm()
    # 페이지에서 어떤 해쉬태그를 클릭했는지 표시하기 위해서 hashtag 도 같이 리턴
    return render(request, "posts/list.html", {"posts":posts,"comment_form":comment_form, "hashtag":hashtag})